require 'rails_helper'

RSpec.describe Tweet, type: :model do
  it { should validate_length_of(:body), maximum: 180 }
  it { should validate_uniqueness_of(:body).scoped_to(:user_id).conditions(created_at: (Time.now - 1.day)..Time.now) }
end
