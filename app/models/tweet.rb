class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  validates_uniqueness_of :user_id, :body, if: -> { where("DATE(created_at) > ?", (Date.today - 1.day)) }
end
